package murach.circle;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println("Welcome to the Circle Calculator");
        System.out.println();

        Scanner sc = new Scanner(System.in);
        
        String choice = "y";
        
        while (choice.equals("y")) {
            // get input from user
            /* 
             *HW4 Ex 2, step 7: Modify the code that gets the radius from
             * the user so ti prompts the user to enter one or more radiuses
             * ( or "radii," if you prefer) on the same line, with each radius
             * seperated by a space. Store this line in a String variable
             * called Line.
            */
            System.out.print("Enter one or more radius values,  seperated by a "
                    + "space: ");
            double radius = Double.parseDouble(sc.nextLine());
            String line = sc.nextLine();
            
            /*
             * HW 4 Ex2, Step 8: Modify the code so it parses the line of text that 
             *the user enters to get a 1-D array of String objects, with one String 
             *object for each entry (you can call the String array entries). 
             *NOTE: We haven't studied String manipulations in Java yet, so to 
             *handle the parsing of the "String of radii" into separate Strings, 
             *you can use the trim and split instance methods of the String class 
             *like this:
            */
            line = line.trim(); // trims off any extra spaces at the start and end 
                                //of line
            String entries[] = line.split(" "); // "tokenizes" or "parses" the 
                                                //String of radii (with radius 
                                                //values separated by spaces) into  
                                                //an array of String objects 
                                                //("tokens") 
            
            // create the Circle object
            Circle circle = new Circle(radius);
            
            /*
            * HW4 Ex2, Step 9:Declare and create a new 1-D array of Circle object 
            * references (which you might call circles), such
            * that the length of circles will have the same length as the entries 
            * array created above.
            */
            
            Circle[] circles = new Circle[2];
            
            /*
            * Write a counter-controlled for loop that iterates through each element
            * in the entries array. For each element in this array, convert the 
            * String referred to by the current entries element into a double, and 
            * store this double in a variable called radius. Use this value of 
            * radius as the argument to the "one-arg" Circle constructor, and store 
            * a reference to your newly-instantiated Circle object in the current 
            * element of the circles array 
            */
           // for (int i=0; i<= entries.length; i++){
           //      radius = Double.parseDouble((entries));
                 
                
          //  } // end for-loop
            
            
            // format and display output
            String message = 
                "Area:          " + circle.getArea() + "\n" +
                "Circumference: " + circle.getCircumference() + "\n" +
                "Diameter:      " + circle.getDiameter() + "\n";
            System.out.println(message);

            // see if the user wants to continue5
            System.out.print("Continue? (y/n): ");
            choice = sc.nextLine();
            System.out.println();
        }
        System.out.println("Bye!");
        sc.close();
    }   
}